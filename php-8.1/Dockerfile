FROM php:8.1

# Install Nodejs using NVM
ENV NVM_DIR=/usr/local/nvm
ENV NODE_VERSION=14.16.1
ENV NODE_PATH=$NVM_DIR/versions/node/v$NODE_VERSION/lib/node_modules
ENV PATH=$NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

RUN mkdir -p $NVM_DIR \
  && cd $NVM_DIR \
  && curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash \
  && . $NVM_DIR/nvm.sh \
  && nvm install $NODE_VERSION \
  && nvm alias default $NODE_VERSION \
  && nvm use default

RUN apt-get update -y && apt-get install -y \
  libmcrypt-dev \
  libonig-dev \
  openssl \
  curl \
  libcurl3-dev \
  zip \
  python3 \
  python3-pip \
  python3-setuptools \
  python3-venv \
  git

# Create and use virtual environment for Python packages
RUN python3 -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

# Now install pip and awscli in the virtual environment
RUN pip3 install --upgrade pip
RUN pip3 --no-cache-dir install --upgrade awscli

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*


RUN docker-php-ext-install pdo pdo_mysql mbstring

RUN pecl install mcrypt-1.0.6
RUN docker-php-ext-enable mcrypt

RUN pecl install xdebug
RUN docker-php-ext-enable xdebug

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Laravel Envoy
RUN composer global require "laravel/envoy=~1.0"

ENV TZ=Europe/London
